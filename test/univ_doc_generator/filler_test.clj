(ns univ-doc-generator.filler-test
  (:require [clojure.test :refer :all]
            [univ-doc-generator.rpd-filler :as rf]))

(deftest test-weeks
  (testing "test-simple-weeks-calcullation"
    (is (= '("1" "2-3" "4-5" "6" "7-8" "9-10" "11" nil)
           (rf/weeks [{"lec" 2.0}
                      {"lec" 2.0, "pr" 4.0, "lab" 4.0}
                      {"lec" 2.0, "pr" 4.0, "lab" 4.0}
                      {"lec" 2.0}
                      {"lec" 2.0, "pr" 2.0, "lab" 4.0}
                      {"lec" 2.0, "pr" 2.0, "lab" 4.0}
                      {"lec" 2.0, "pr" 2.0}
                      {}] 17))))

  (testing "test-weeks-with-megelections"
    (is (= '("1" "2" "3" "4-8" "9" "10" "11" nil)
           (rf/weeks [{"lec" 2.0}
                   {"lec" 2.0, "pr" 4.0, "lab" 4.0}
                   {"lec" 2.0, "pr" 4.0, "lab" 4.0}
                   {"lec" 20.0}
                   {"lec" 2.0, "pr" 2.0, "lab" 4.0}
                   {"lec" 2.0, "pr" 2.0, "lab" 4.0}
                   {"lec" 2.0, "pr" 2.0}
                   {}] 17)))))