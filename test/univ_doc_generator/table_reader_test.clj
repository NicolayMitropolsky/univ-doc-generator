(ns univ-doc-generator.table-reader-test
  (:require [clojure.test :refer :all]
            [univ-doc-generator.table-reader :refer :all]))

(deftest test-reads-a-map
  (testing "getting all data from testBook1.xls"
    (is (= {"A"  (list {"поле1" "abc"
                        "поле2" "Тут текст"
                        "поле3" 1.0}
                       {"поле1" "cde"
                        "поле2" "Текс"
                        "поле3" 1.4}
                       {"поле1" "Привет"
                        "поле2" "Тексу"
                        "поле3" 5.0})
            "B"  (list {"значение"    12.0
                        "ключ"        "Вася"
                        "комментарий" 13.4}
                       {"значение"    56.0
                        "ключ"        "Петя"
                        "комментарий" "ПР1"}
                       {"значение" 3.0
                        "ключ"     "Маша"}
                       {"значение" 4.0})
            "L1" (list "Пункт1"
                       "Пункт2"
                       "Пункт3")
            "V1" 12.0} (get-spreadsheet-data "test/univ_doc_generator/testBook1.xlsx")))))
