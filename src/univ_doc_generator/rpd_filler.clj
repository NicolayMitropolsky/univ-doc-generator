(ns univ-doc-generator.rpd-filler
  (:require [univ-doc-generator.core :as c]
            [univ-doc-generator.table-reader :as tr]
            [clojure.walk :as walk]
            [univ-doc-generator.utils.utils :refer :all]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [univ-doc-generator.utils.files :as fl]
            [univ-doc-generator.utils.utils :as u]))

(def MERGE-SYMBOL "\u21DE")
(def MERGE-PATTERN (re-pattern (str MERGE-SYMBOL "+")))

(defn non-breakable [s]
  (some-> s (str/replace \- \u2011)))

(defn get-or [data & keys]
  (->> keys (map #(get data %)) (remove nil?) (first)))

(defn try-get-suffixed [data key i]
  (get-or data (str key i) key))

(defn competencies-to-map [data]
  (->> (u/check-not-null data "data")
       (filter #(>= (count %) 2))
       (map (fn [{:strs [code descr content]}]
              [(non-breakable code)
               {"descr"        descr "content" content
                "content-data" (some->> content (str/split-lines)
                                    (mapcat c/subcompetenecies)
                                    (remove nil?))
                }]))
       (into {})))

(defn productive-section? [content-entry] (some? (content-entry "content")))

(defn indexing [seq-of-map] (map-indexed (fn [i e] (assoc e "n" (inc i))) seq-of-map ))

(defn ->Double [arg]
  (cond (double? arg) arg
        (number? arg) (double arg)
        (string? arg) (Double/parseDouble arg)
        :else (throw (IllegalArgumentException. (str "Cant convert '" arg "' to double")))))

(defn split-commas [str]
  (cond (nil? str) nil
        (string? str) (some-> str (str/split #",\s*"))
        (seq? str) str
        :else [str]))

(defn sum-by-keys [keys content]
  (some->> content (mapcat #(vals (select-keys % keys))) (not-empty) (reduce +)))

(defn weeks [content max-week]
  (let [hours-per-week (array-map "lec" 2 "pr" 2 "lab" 2)
        countize (fn [section] (mapv (fn [[t h]] (-> (get section t 0) (/ h) (int))) hours-per-week))
        maxs-in-sec (map (fn [section] (reduce max (countize section))) content)
        normal-weeks-required (reduce + maxs-in-sec)
        multiplier (inc (unchecked-divide-int normal-weeks-required max-week))
        intervals (->> maxs-in-sec
                       (reductions
                         (fn [[_ start] sec-length]
                           (let [sec-length-actual (-> sec-length (/ multiplier) (Math/ceil) (int))]
                             [(inc start) (+ start sec-length-actual)])) [0 0])
                       (rest))]
    (map
      (fn [[begin end]]
        (cond (> begin end) nil
              (= begin end) (str begin)
              :else (str begin "-" end)))
      intervals)))

(defn section-has-rcode [section rcode-info]
  (and
    (some (partial = (non-breakable (rcode-info "code"))) (split-commas (non-breakable (section "comp"))) )
    (not (some (partial = (non-breakable (rcode-info "rcode"))) (sequence (split-commas (non-breakable (section "skip-kk"))))))))

(defn handle-type [spreadsheet-data id]
  (let [content (spreadsheet-data "content")
        ids (str id "s")
        ids-content (or (spreadsheet-data (str ids "_content"))
                        (->> content (filter #(get % id))))]
    (->> ids-content
         (map (fn [entry]
                {"nums"    (or (entry "nums") (entry "num"))
                 "hours"   (or (entry "hours") (entry id))
                 "name"    (or (entry "name") (entry "section"))
                 "control" (entry "control")}))
         (indexing))))

(defn control-info [spreadsheet-data rdata section]
  (let [mode (rdata "mode")
        questions (spreadsheet-data "questions")
        check-controls (fn [data]
                         (comment (when (empty? data)
                                    (println "WARN: нет контроля для "
                                             (->> (rdata "rcodes") (str/join ", ")) " в " "#" (section "num") "(" (section "section") ")"))))]
    (case mode
      "Знать" (some->> questions (filter #(= (% "section") (section "num")))
                       (map #(int (% "n")))
                       (apply-pre check-controls)
                       (seq)
                       (ranges-string)
                       (array-map "questions"))
      "Владеть" (some->> (spreadsheet-data "labs")
                         (filter #(= (% "nums") (section "num")))
                         (apply-pre check-controls)
                         (seq) (array-map "labs"))
      "Уметь" (some->> (spreadsheet-data "prs")
                       (filter #(= (% "nums") (section "num")))
                       (apply-pre check-controls)
                       (seq) (array-map "prs")))))

(defn fos-data [spreadsheet-data competencies competences_rcodes]
  (let [content (spreadsheet-data "content")
        questions (spreadsheet-data "questions")
        questions-count (frequencies (->> questions (map #(get % "section"))))
        competencies-by-subcode (->> competencies
                                     (map (fn [[k v]] (assoc v "code" k)))
                                     (mapcat (fn [comp]
                                               (->> (comp "content-data")
                                                    (map (fn [subcomp]
                                                           [(subcomp "subcode") {"content"      (subcomp "content")
                                                                                 "parent_descr" (comp "descr")
                                                                                 "parent-code"  (comp "code")}])))))
                                     (into {}))
        sections-names (->> content (map (fn [sec] [(sec "num") (sec "section")])) (into {}))
        indicators-data (->> (spreadsheet-data "questions")
                             (mapcat (fn [q] (some->> (split-commas (q "subcomps"))
                                                  (map (fn [sc] [sc (select-keys q ["n", "section"])])))))
                             (group-and-aggregate first second))]
    {
     "questions_list"  (->> questions (map #(get % "text")))
     "used-content"    questions-count
     "passport"        (->> (for [section (filter productive-section? content)
                                  :let [skip-kk (set (split-commas (section "skip-kk")))]
                                  comp (split-commas (section "comp"))
                                  rdata competences_rcodes
                                  :when (= comp (rdata "code"))
                                  :let [mode (rdata "mode")
                                        rdata (assoc rdata "rcodes" (remove #(skip-kk (% "rcode")) (rdata "rcodes")))
                                        control-info (control-info spreadsheet-data rdata section)]
                                  :when (and (not-empty (rdata "rcodes")) control-info)]
                              (merge {"num"   (section "num")
                                      "name"  (section "section")
                                      "comp"  comp
                                      "descr" ((competencies comp) "descr")
                                      "mode"  mode
                                      "zyv"   (rdata "rcodes")}
                                     control-info))
                            (map (dedupe-keys ["comp" "descr"] MERGE-SYMBOL))
                            (map (dedupe-keys ["num" "name"] MERGE-SYMBOL)))
     "sections_comps_" (->> indicators-data
                            (mapcat (fn [[sc datas]]
                                      (->> datas (map
                                                   (fn [data]
                                                     {"section" (data "section")
                                                      "comp"    ((competencies-by-subcode sc) "parent-code")
                                                      }
                                                     )))))
                            (u/group-and-aggregate #(% "section") #(% "comp"))
                            (map (fn [[k vs]] [(int k) (->> vs (into (sorted-set-by c/comp-code-num-comparator)) (str/join ", "))]))
                            (sort-by first)
                            (map second) (str/join "\n"))
     "indicators"      (->> indicators-data
                            (map (fn [[sc data]]
                                   (apply merge-with conj {"subcode" sc "n" [] "section" []} data)))
                            (map (fn [data]
                                   (let [subcode-info (competencies-by-subcode (data "subcode"))]
                                     {"subcode"       (data "subcode")
                                      "descr"         (subcode-info "content")
                                      "parent_descr"  (subcode-info "parent_descr")
                                      "questions_all" (apply sorted-set (data "n"))
                                      "questions_str" (ranges-string (map int (data "n")))
                                      "sections_nums" (apply sorted-set (data "section"))
                                      "sections_str"  (->> (apply sorted-set (data "section")) (map int) (ranges-string))})))
                            (sort-by #(get % "subcode"))
                            (map (u/dedupe-key "parent_descr" "")))
     "total_tasks"     (count questions)
     "ticket1topics"   (->> (spreadsheet-data "ticket1topics")
                            (map (fn [n] (get (->> questions (filter #(-> % (get "n") (= n))) (first)) "text"))))
     "labs_tasks"      (->> (spreadsheet-data "labs")
                            (keep (fn [lab]
                                    (some->> (spreadsheet-data (str "lab" (lab "n"))) (map #(array-map "text" %)) (indexing) (assoc lab "tasks")))))
     "prs_controls"    (->> (spreadsheet-data "prs")
                            (keep (fn [pr]
                                    (let [sections-nums (set (map ->Double (split-commas (pr "nums"))))
                                          sections (filter (fn [sec] (sections-nums (sec "num"))) content)]
                                      (merge pr
                                             {"text" (->> sections (map (fn [sec] (or (not-empty (sec "questions")) (not-empty (sec "content")))))
                                                          (str/join " "))})))))
     })
  )

(defn fill-rpd-datas [subjname]
  (let [spreadsheet-data (tr/get-spreadsheet-data (str "subjs/" subjname ".xlsx"))
        content (spreadsheet-data "content")
        controls (->> content (filter #(#{"зачет" "экзамен"} (% "comp"))))]
    (or (->> (spreadsheet-data "directions")
             (map (fn [dn direction]
                    (let [spreadsheet-data (-> spreadsheet-data
                                               (update-in ["content"]
                                                          (fn [content]
                                                            (->> content
                                                                 (map (fn [entry]
                                                                        (->> dn (try-get-suffixed entry "comp") (non-breakable) (assoc entry "comp"))))
                                                                 (map (fn [entry]
                                                                        (->> dn (try-get-suffixed entry "skip-kk") (non-breakable) (assoc entry "skip-kk")))))))
                                               (merge {"labs" (handle-type spreadsheet-data "lab")
                                                       "prs"  (handle-type spreadsheet-data "pr")}))
                          content (spreadsheet-data "content")
                          competencies-ss-data (tr/get-spreadsheet-data "subjs/competentions.xlsx")
                          kurs (-> (->> content (map #(get % "semester")) (apply min)) (/ 2) (int))
                          competencies (competencies-to-map (try-get-suffixed competencies-ss-data "comps" dn))
                          compentences-mapped (->> content
                                                   (mapcat #(split-commas (% "comp")))
                                                   (map non-breakable)
                                                   (sort)
                                                   (distinct)
                                                   (c/competencies-mapped competencies))
                          rcodes (->> (try-get-suffixed competencies-ss-data "kk" dn)
                                      (map (fn [rcode-record]
                                             (merge rcode-record (-> rcode-record (get "rcode") (c/parse-comp-r-code)))))
                                      (group-by #(get % "code")))
                          compentences-all (sort-by c/comp-code-num-sorter (flatten (vals compentences-mapped)))
                          competences_rcodes (->> compentences-all
                                                  (mapcat
                                                    (fn [comp]
                                                      (->> (get rcodes (get comp "code"))
                                                           (filter (fn [rdi] (some #(section-has-rcode % rdi) content)))
                                                           (group-by #(get % "mode"))
                                                           (map (fn [[mode rcds]]
                                                                  {"code"   (get comp "code")
                                                                   "descr"  (get comp "descr")
                                                                   "mode"   mode
                                                                   "rcodes" rcds
                                                                   }))))))]
                      (merge spreadsheet-data
                             {
                              "sems"               (->> content (map #(->> (% "semester") (int))) (distinct) (str/join ", "))
                              "direction"          direction
                              "chair"              (try-get-suffixed competencies-ss-data "chair" dn)
                              "chair_head"         (try-get-suffixed competencies-ss-data "chair_head" dn)
                              "competences"        compentences-mapped
                              "competences_rcodes" (->> competences_rcodes
                                                        (map (u/dedupe-keys ["code" "descr"] MERGE-SYMBOL)))
                              "competences_all"    compentences-all
                              "pred_competences"   (->> (try-get-suffixed spreadsheet-data "pre_competencies" dn)
                                                        (split-commas)
                                                        (map non-breakable)
                                                        (sort-by c/comp-code-num-sorter)
                                                        (c/competencies-mapped competencies))
                              "sum"                {"total"   (sum-by-keys ["self" "lec" "pr" "lab" "control"] content)
                                                    "contact" (sum-by-keys ["lec" "pr" "lab"] content)
                                                    "lec"     (sum-by-keys ["lec"] content)
                                                    "pr"      (sum-by-keys ["pr"] content)
                                                    "lab"     (sum-by-keys ["lab"] content)
                                                    "self"    (sum-by-keys ["self"] content)
                                                    "control" (sum-by-keys ["control"] content)}
                              "content"            (->> content
                                                        (map merge (map #(array-map "weeks" %) (weeks content (get spreadsheet-data "weeks-max" 17))))
                                                        (filter productive-section?))
                              "content_listable"   (->> content (filter productive-section?))
                              "control"            ((last controls) "comp")
                              "kurs"               kurs
                              "group_year"         (- 18 kurs)
                              }
                             (fos-data spreadsheet-data competencies competences_rcodes)))) (iterate inc 1)))
        (throw (IllegalStateException. (str "no data for " subjname " maybe you missed the \"directions\" field?"))))))


(defn fill-rpd
  ([template subjname data-or-index suffix]
   (println "fiiling the" subjname suffix)
   (let [data (if (map? data-or-index)
                data-or-index
                (nth (fill-rpd-datas subjname) (dec data-or-index)))]
     (c/fill-template template (fold-keys data) (str subjname "-" suffix) {:merge-pattern MERGE-PATTERN} ))))

(defn gen-all-for-sublj [subjname]
  (doseq [data (fill-rpd-datas subjname)]
    (let [subjname (data "name")
          acronim (acronimyse (data "direction"))]
      (fill-rpd "РПД-template.docx" subjname data (str acronim "-" "РПД.docx"))
      (fill-rpd "ФОС-template.docx" subjname data (str acronim "-" "ФОС.docx")))))

(defn gen-all []
  (doseq [subjname (->> "subjs" (io/file) (.listFiles) (seq)
                        (map #(.getName %))
                        (map #(.substring % 0 (.lastIndexOf % ".")))
                        (filter #(and (not-empty %) (Character/isUpperCase ^Character (nth % 0)))))]
    (gen-all-for-sublj subjname)))


(defn topics-and-comps [data]
  (let [comps-by-code (->> (data "competences_all") (map (fn [comp] [(comp "code") (select-keys comp ["descr" "content"])])) (into {}))]
    (->> (data "content")
         (map #(select-keys % ["section" "comp" "num"]))
         (map (fn [section]
                (merge section
                       {"comps" (->> (split-commas (section "comp")) (map comps-by-code))}))))))

(defn link-from-onedrive3+
  ([uits-path]
   (sh/sh "mkdir" "subjs")
   (fl/symlink uits-path "subjs" "competentions.xlsx")
   (fl/symlink uits-path "" "РПД-template.docx")
   (fl/symlink uits-path "" "ФОС-template.docx")
   (doseq [rpd (->> (fl/ls uits-path)
                    (filter #(re-matches #".*/РПД-.*\.xlsx" %)))]
     (fl/symlink rpd (->> (io/file rpd) (.getName) (fl/join-paths "subjs")))))
  ([] (link-from-onedrive3+ (fl/join-paths (System/getProperty "user.home") "OneDrive/УИТС-2018"))))

(defn link-from-onedrive3++
  ([uits-path]
   (sh/sh "mkdir" "subjs")
   (fl/symlink uits-path "subjs" "competentions.xlsx")
   (fl/symlink (fl/join-paths uits-path "ФГОС3++") "" "РПД-template.docx")
   (fl/symlink (fl/join-paths uits-path "ФГОС3++") "" "ФОС-template.docx")
   (doseq [rpd (->> (fl/ls uits-path)
                    (filter #(re-matches #".*/РПД-.*\.xlsx" %)))]
     (fl/symlink rpd (->> (io/file rpd) (.getName) (fl/join-paths "subjs")))))
  ([] (link-from-onedrive3++ (fl/join-paths (System/getProperty "user.home") "OneDrive/УИТС-2019"))))