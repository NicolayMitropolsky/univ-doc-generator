(ns univ-doc-generator.utils.field-corrector
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [univ-doc-generator.utils.xml :refer :all]
            [univ-doc-generator.utils.utils :as u])
  (:import (fr.opensagres.xdocreport.document.preprocessor.dom DOMPreprocessor)
           (fr.opensagres.xdocreport.template.formatter IDocumentFormatter FieldsMetadata)
           (fr.opensagres.xdocreport.document.docx DocxConstants)
           (java.util Map)
           (javax.xml.parsers DocumentBuilderFactory)
           (org.w3c.dom Document Node NodeList Element)
           (fr.opensagres.xdocreport.core.utils DOMUtils)
           (univdocgen Formatter)
           (fr.opensagres.xdocreport.document IXDocReport)
           (fr.opensagres.xdocreport.core.io XDocArchive)
           (java.io ByteArrayOutputStream InputStream OutputStream)))


(defn save [^Document document]
  (with-open [out (io/writer "processed.xml")]
    (DOMUtils/save document out)))

(defn- fldChar-container-value [^Node node]
  (when (tag-name= "r" node)
    (->> node
         (children-seq)
         (filter (tag-name= "fldChar"))
         (keep (fn [node]
                 (attr-val "fldCharType" node)))
         (first))))

(defn- fldChar-container? [^String valname ^Node node] (= valname (fldChar-container-value node)))

(defn- get-id [node] (attr-val "id" node))

(defn- get-ender-fn [begin]
  (cond
    (tag-name= "bookmarkStart" begin)
    (let [id (get-id begin)] (fn [next]
                               (case (tag-name next)
                                 "bookmarkEnd" (when (= id (get-id begin)) ::block-end)
                                 nil)))

    (fldChar-container? "begin" begin)
    (fn [next]
      (case (fldChar-container-value next)
        "begin" ::block-start
        "end" ::block-end
        nil))))

(defn- take-util-pairing-end [ender ^Node bookMarkStart]
  (loop [nodes [bookMarkStart] nesting 0]
    (if-let [next (.getNextSibling (last nodes))]
      (case (ender next)
        ::block-start (recur (conj nodes next) (inc nesting))
        ::block-end (if (> nesting 0)
                      (recur (conj nodes next) (dec nesting))
                      (conj nodes next))
        (recur (conj nodes next) nesting))
      nodes)))

(defn find-fields-to-merge [docx]
  (->> docx
       (all-nodes)
       (filter #(instance? Element %))
       (keep (fn [begin]
               (when-let [ender (get-ender-fn begin)]
                 (with-meta (take-util-pairing-end ender begin) {:begin-tag (tag-name begin) :id (get-id begin)}))))))


(defn- find-defaults-to-delete [docx]
  (->> docx
       (all-nodes)
       (filter #(instance? Element %))
       (filter #(hasParentTags '("default" "textInput" "ffData" "fldChar" "r") %))))

(defn- convert-to-merge-field [prefixed]
  (doseq [prefixed prefixed]
    (let [fld (->> prefixed (parents) (filter (tag-name= "fldChar")) (first))
          parent (.getParentNode fld)
          simple (.createElement (.getOwnerDocument parent) "w:fldSimple")]
      (.setAttribute simple "w:instr" (str " MERGEFIELD  " (.getAttribute prefixed "w:val") "  \\* MERGEFORMAT "))
      (.insertBefore parent simple fld))))

(defn inner-texts [noProofs] (mapcat (fn [r] (->> r (children-seq) (filter (tag-name= "t")))) noProofs))

(defn preprocessor []
  (proxy [DOMPreprocessor] []
    (visit [^Document document, ^String entryName, ^FieldsMetadata fieldsMetadata,
            ^IDocumentFormatter formatter, ^Map sharedContext]
      (doseq [fields (->> (find-fields-to-merge document) (filter #(> (count %) 1)))]
        (let [inner-t (inner-texts fields)
              joined-text (->> inner-t (map #(.getTextContent %)) (str/join))]
          (when (seq inner-t)
            (some-> (first inner-t) (.setTextContent joined-text))
            (remove-nodes (rest inner-t)))))

      (remove-nodes (->> document (all-nodes)
                         (filter (tag-name= "t"))
                         (filter (fn [node] (some #(str/starts-with? (.getTextContent node) %) ["@before-row" "@after-row"])))))

      (let [defaults (find-defaults-to-delete document)]
        (convert-to-merge-field (filter #(-> % (.getAttribute "w:val") (str/starts-with? "@")) defaults))
        (remove-nodes defaults)))))

(defn modify-entry [archive entry processor]
  (with-open [input (-> archive (.getEntryInputStream entry))
              output (ByteArrayOutputStream.)]
    (processor input output)
    (XDocArchive/setEntry archive entry (io/input-stream (.toByteArray output)))
    (.toByteArray output)))

(defn format-archive-entry [arhive entry]
  (condp instance? arhive
    IXDocReport (format-archive-entry (.getPreprocessedDocumentArchive arhive) entry)
    XDocArchive (let [modified (modify-entry arhive entry (fn [^InputStream input ^OutputStream out] (Formatter/formatXml input out)))]
                  (io/copy (io/input-stream modified) (io/output-stream "out.xml")))))

(defn docx-xml ^InputStream [docxFile]
  (-> docxFile (io/input-stream) (XDocArchive/readZip) (.getEntryInputStream DocxConstants/WORD_DOCUMENT_XML_ENTRY)))

(defn format-docx-xml [docx out-xml]
  (with-open [out (io/output-stream out-xml)
              docx (docx-xml docx)]
    (univdocgen.Formatter/formatXml docx out)))


(defn tmap [f table]
  (when table
    (->> table
         (map-indexed
           (fn [r row]
             (->> row (map-indexed (fn [c coll] (f coll r c))) (vec))))
         (vec))))

(defn tkeep [f table]
  (when table
    (->> table
         (map-indexed
           (fn [r row]
             (->> row (map-indexed (fn [c coll] (f coll r c))) (filter some?))))
         (apply concat)
         (vec))))

(defn table
  ([tbl]
   (when (= "tbl" (tag-name tbl))
     (->> tbl (all-nodes) (filter (tag-name= "tr"))
          (mapv (fn [tr]
                  (->> tr (all-nodes) (filterv (tag-name= "tc"))))))))
  ([f tbl] (tmap f (table tbl))))

(defn merge-table-cells [^Element tbl merge-marker]
  (when-let [table (table tbl)]
    (let [doc (.getOwnerDocument tbl)
          tmerge (tmap (fn [n r c] (some? (re-matches merge-marker (get-t-content n)))) table)
          marked-cells (tkeep (fn [v r c] (when v [r c])) tmerge)
          merge-parents (->> marked-cells (keep (fn [[r c]] (when-not (get-in tmerge [(dec r) c] true) [(dec r) c]))))
          insert-merge (fn [inds val]
                         (doseq [^Element mp (->> inds (map #(get-in table %)) (keep (fn [tc] (->> tc (children-seq) (filter (tag-name= "tcPr")) (first)))))]
                           (let [mergeElement (.createElement doc "w:vMerge")]
                             (when val (.setAttribute mergeElement "w:val" val))
                             (.appendChild mp mergeElement))))]
      (insert-merge merge-parents "restart")
      (insert-merge marked-cells nil))))


(defn merge-table-cells-in-doc [arhive-file pattern]
  (let [arhive (XDocArchive/readZip (io/input-stream arhive-file))]
    (modify-entry arhive DocxConstants/WORD_DOCUMENT_XML_ENTRY
                  (fn [input ^OutputStream output]
                    (let [^Document d (-> (DocumentBuilderFactory/newInstance) .newDocumentBuilder (.parse input))]
                      (doall (keep #(merge-table-cells % pattern) (all-nodes d)))
                      (DOMUtils/save d output))))
    (XDocArchive/writeZip arhive (io/output-stream arhive-file))))
