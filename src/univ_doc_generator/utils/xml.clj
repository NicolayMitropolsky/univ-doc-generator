(ns univ-doc-generator.utils.xml
  (:require [clojure.string :as str])
  (:import (org.w3c.dom NamedNodeMap Node NodeList Element)))

(defn- lastQualified [^String name ] (-> name (str/split #":") (last)))

(defn siblings [^Node node] (->> node (iterate #(.getNextSibling ^Node %)) (take-while some?)))

(defn parents [^Node node] (->> node (iterate #(.getParentNode ^Node  %)) (take-while some?)))

(defn nodelist->seq [^NodeList nodelist] (when nodelist (map #(.item nodelist %) (range (.getLength nodelist)))))

(defn namednodemap->seq [^NamedNodeMap nodelist] (when nodelist (map #(.item nodelist %) (range (.getLength nodelist)))))

(defn children-seq [^Node node] (nodelist->seq (.getChildNodes node)))

(defn attr-seq [^Node node] (namednodemap->seq (.getAttributes node)))

(defn attr [^String name  ^Node node] (->> (attr-seq node) (filter #(= name (lastQualified (.getNodeName ^Node %)))) (first)))

(defn attr-val [^String name ^Node node] (some->> node (attr name) (.getNodeValue)))

(defn all-nodes [^Node node]
  (cons node (lazy-seq (mapcat all-nodes (children-seq node)))))

(defn remove-nodes [node]
  (doseq [^Node node node]
    (-> node (.getParentNode) (.removeChild node))))

(declare tagName=)

(defn tag-name [arg]
  (condp instance? arg
    Element (let [^Element arg arg] (lastQualified (.getTagName arg)))
    nil))

(defn tag-name=
  ([expected-name] (fn [node] (tag-name= expected-name node)))
  ([expected-name node]
   (= expected-name (tag-name node))))

(defn hasParentTags [tags node]
  (->> node
       (parents)
       (filter #(instance? Element %))
       (map tag-name)
       (take (count tags))
       (= tags)))

(defn get-t-content [n]
  (->> n (all-nodes) (filter (tag-name= "t")) (map #(.getTextContent %)) (str/join)))