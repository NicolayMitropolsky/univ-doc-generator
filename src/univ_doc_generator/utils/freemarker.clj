(ns univ-doc-generator.utils.freemarker
  (:require [clojure.string :as str]
            [univ-doc-generator.utils.field-corrector :as fc]
            [instaparse.core :as insta])
  (:import (java.util Enumeration)
           (freemarker.template Template)
           (fr.opensagres.xdocreport.document XDocReport IXDocReport)
           (fr.opensagres.xdocreport.document.docx DocxConstants)
           (javax.xml.parsers DocumentBuilderFactory)
           (org.w3c.dom Document)))


(defn- preprocess-if-needed [template]
  (when-not (.isPreprocessed template) (.setDocumentArchive template (.getPreprocessedDocumentArchive template))))

(defn fr-template [^IXDocReport template]
  (preprocess-if-needed template)

  (Template. DocxConstants/WORD_DOCUMENT_XML_ENTRY
             (-> template (.getPreprocessedDocumentArchive) (.getEntryReader DocxConstants/WORD_DOCUMENT_XML_ENTRY))
             (-> template (.getTemplateEngine) (.getFreemarkerConfiguration))))

(defn children [node] (iterator-seq (.asIterator ^Enumeration (.children node))))

(defn allChildren [node] (cons node (mapcat #(allChildren %) (children node))))

(defn document [^IXDocReport template]
  (preprocess-if-needed template)
  (with-open [entry (-> template (.getPreprocessedDocumentArchive) (.getEntryInputStream DocxConstants/WORD_DOCUMENT_XML_ENTRY))]
    (-> (DocumentBuilderFactory/newDefaultInstance) (.newDocumentBuilder) (.parse entry))))


(defn fr-elements [^Document docx]
  (->> (fc/find-fields-to-merge docx)
       (map (fn [field] (->> (fc/inner-texts field) (map #(.getTextContent %)) (str/join))))
       (filter (fn [s] (some #(str/starts-with? s %) ["${" "[#" "[/#"]) ))))


(defn fr-parser (insta/parser
                  "elem = (list|ref|<whitespace>)*
                   ref = <'${'>(identifier|identifier-with-default)<'}'>
                   <identifier-with-default> = <'('> identifier <')'> ['!' default-value]
                   default-value = #'\\S'+?
                   list = list-start <whitespace>? elem* <whitespace>? list-end
                   items = <'[#items'> <whitespace> as? <whitespace>? <']'> <whitespace>? elem* <whitespace>? <'[/#items]'>
                   <list-start> = <'[#list'> <whitespace> identifier <whitespace> as? <whitespace>? <']'>
                   <list-end> = <'[/#list]'>
                   as = <'as'> <whitespace> identifier
                   identifier = #'\\w+' ('.' #'\\w+')*
                   <whitespace> = #'\\s+'
                   <letters> = #'[a-zA-Z]'+
                   <digits> = #'[0-9]'+"))
