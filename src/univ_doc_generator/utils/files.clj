(ns univ-doc-generator.utils.files
  (:require [clojure.java.shell :as sh]
            [clojure.string :as str]
            [clojure.java.io :as io])
  (:import (java.nio.file Paths)))

(defn join-paths [p & ps]
  (.toString (.normalize (Paths/get p (into-array String ps)))))

(defn ls
  ([path]
   (->> (sh/sh "ls" path) (:out) (str/split-lines) (map #(join-paths path %))))
  ([path1 path2 & ps] (ls (apply join-paths (concat [path1 path2] ps)))))

(defn symlink
  ([source link]
               (sh/sh "ln" "-s" "-f" source link))
  ([source-dir link-dir file-name]
    (symlink (join-paths source-dir file-name) (join-paths link-dir file-name))))


