(ns univ-doc-generator.utils.utils
  (:require [clojure.string :as str]))

(defn check-not-null [exp & message]
  (if-not exp (throw (IllegalArgumentException. ^String (apply str message))))
  exp)

(defn log [msg & arg]
  (apply println (cons msg arg))
  (last arg))

(defn apply-pre [fn arg]
  (fn arg)
  arg)

(defn also [arg fn] (apply-pre fn arg))

(defn getting [key] (fn [m] (get m key)))

(defn fold-keys
  ([m] (fold-keys nil m))
  ([prefix m] (let [prefix_ (if prefix (str prefix "_") "")]
                (if (map? m)
                  (->> (seq m) (map (fn [[k v]] (fold-keys (str prefix_ k) v))) (into {}))
                  [prefix m]))))

(defn acronimyse [str]
  (->> (str/split str #"\s+") (map #(.substring % 0 1)) (map #(.toUpperCase %)) (str/join)))

(defn group-and-aggregate [criteria vals-op coll]
  (->> coll (group-by criteria) (map (fn [[k v]] [k (map vals-op v)])) (into {})))


(defn clustered [nums]
  (let [accepts (fn [set e] (or (empty? set) (-> e (- (last set)) (<= 1))))]
    (loop [accumulated [] pending (sorted-set) remains (sort nums)]
     (let [cur (first remains)]
       (cond (nil? cur) (conj accumulated pending)
             (accepts pending cur) (recur accumulated (conj pending cur) (rest remains))
             :else (recur (conj accumulated pending) (sorted-set cur) (rest remains)))))))

(defn ranges-string [nums]
  (->> (clustered nums)
       (map (fn [cluster] (case (count cluster)
                            0 "<empty>"
                            1 (str (first cluster))
                            (str (first cluster) "-" (last cluster)))))
       (str/join ", ")))

(defn with-state [m]
  (let [state (volatile! nil)]
   (fn [arg]
     (let [[ns r] (m @state arg)]
       (vreset! state ns) r))))

(defn dedupe-keys [keys rm-val]
  (let [subsitution (into {} (map (fn [k] [k rm-val]) keys))]
    (with-state (fn [state data] [(select-keys data keys) (if (= state (select-keys data keys)) (merge data subsitution) data)]))))

(defn dedupe-key [key rm-val]
  (with-state (fn [state data] [(data key) (if (= state (data key)) (assoc data key rm-val) data ) ])))