(ns univ-doc-generator.table-reader
  [:require [dk.ative.docjure.spreadsheet :as sp]]
  (:import (org.apache.poi.xssf.usermodel XSSFCell XSSFSheet)))

(def declaration #"\(def(\w+) (\w+).*\)")

(defn- some-if [pred v] (when (pred v) v))

(defn- cell-at [sheet r c] (some-> sheet (.getRow r) (.getCell c)))

(defn extract-table [^XSSFCell cell]
  (let [^XSSFSheet sheet (.getSheet cell)
        row (.getRowIndex cell)
        col (.getColumnIndex cell)
        headings (->> (iterate inc col)
                      (map #(cell-at sheet (inc row) %))
                      (take-while some?)
                      (map sp/read-cell))]
    (->> (+ row 2) (iterate inc)
         (map (fn [r]
                (->> headings
                     (map-indexed
                       (fn [hn hv]
                         (some->> (cell-at sheet r (+ col hn))
                                  (sp/read-cell)
                                  (conj [hv]))))
                     (into {}))))
         (take-while #(first %)))))

(defn extract-list [^XSSFCell cell]
  (let [^XSSFSheet sheet (.getSheet cell)
        row (.getRowIndex cell)
        col (.getColumnIndex cell)]
    (->> (iterate inc (inc row))
         (map #(cell-at sheet % col))
         (take-while some?)
         (map sp/read-cell)
         (take-while some?))))

(defn extract-val [^XSSFCell cell]
  (let [^XSSFSheet sheet (.getSheet cell)
        row (.getRowIndex cell)
        col (.getColumnIndex cell)]
    (sp/read-cell (cell-at sheet row (inc col)) )))

(defn get-spreadsheet-data [path]
  (let [wb (sp/load-workbook path)]
    (->> wb (sp/sheet-seq)
         (sp/cell-seq)
         (mapcat (fn [cell]
                   (when-let [[_ type name] (some->> (sp/read-cell cell) (some-if string?) (re-matches declaration))]
                     (when-let [extractor (case type
                                            "table" extract-table
                                            "list" extract-list
                                            "val" extract-val)]
                       {name (extractor cell)}))))
         (into {}))))

