(ns univ-doc-generator.core
  (:require [clj-xpath.core :as xp]
            [clojure.string :as string]
            [clojure.java.io :as io]
            [clojure.java.io :as io]
            [clojure.walk :as walk]
            [clojure.xml :as xml]
            [univ-doc-generator.utils.field-corrector :as fc]
            [hawk.core :as hawk]
            [univ-doc-generator.utils.utils :refer :all]
            [clojure.string :as str])
  (:import (fr.opensagres.xdocreport.document.registry XDocReportRegistry)
           (fr.opensagres.xdocreport.template TemplateEngineKind)
           (fr.opensagres.xdocreport.template TemplateEngineKind)
           (fr.opensagres.xdocreport.document IXDocReport)
           (fr.opensagres.xdocreport.core.io XDocArchive)
           (javax.xml.parsers DocumentBuilderFactory)
           (fr.opensagres.xdocreport.document.docx DocxConstants)
           (fr.opensagres.xdocreport.document.docx.preprocessor.sax DocxPreprocessor)
           (java.util Map HashMap)
           (java.io InputStream)))


(defn validXml [docxFile]
  (try (-> (DocumentBuilderFactory/newInstance) .newDocumentBuilder (.parse (fc/docx-xml docxFile)))
       (catch Exception e (throw (Exception. (str "error processing " docxFile) e)))))

(defn comp-code-num [comp] (let [[name num] (string/split comp #"[-\u2011]") ]
                             [name (Integer/parseInt num)]))

(def ^:private comp-name-order {"ОК"  1
                                "УК"  1
                                "ПК"  3
                                "ОПК" 2
                                "ПСК" 4})

(defn comp-code-num-sorter [comp]
  (cond
    (map? comp)
    (comp-code-num-sorter (comp "code"))
    (string? comp)
    (let [[name num] (string/split comp #"[-\u2011]")]
      [(or (comp-name-order name) name) (some-> num (Integer/parseInt))])))

(defn comp-code-num-comparator [x y]
  (let [a (comp-code-num-sorter x) b (comp-code-num-sorter y)] (compare a b)))

(defn subcompetenecies [content]
  (->> content (str/split-lines)
       (map
         (fn [line]
           (let [m (re-matcher #"\p{Lu}{2,3}[-\u2011]\d+\.\d+" line)]
             (when (.find m)
               {"subcode" (.substring content 0 (.end m))
                "content" content}))))))

(defn competenceClass [comp] (first (comp-code-num comp)))

(defn competencies-with-codes [competences-map comps]
  (->> comps
       (keep (fn [comp]
               (when-let [description (get competences-map comp)]
                 (if (map? description)
                   (merge description {"code" comp})
                   {"code"  comp,
                    "descr" (string/replace description "способностью" "способность")}))))))

(defn parse-comp-r-code [^String code]
  (let [m (re-matcher #"(\p{Lu}{2,3})[-\u2011](\d+)[-\u2011](\p{Lu})(\d+)" code)]
    (when-let [[_ name num mode pos] (re-find m)]
      {"code"  (str name \u2011 num)
       "mode"  (case mode
                 "З" "Знать"
                 "В" "Владеть"
                 "У" "Уметь")
       "pos"   pos})))

(defn process [^IXDocReport template data output-file]
  (with-open [out (io/output-stream output-file)]
    (.process template (HashMap. ^Map data) out)))


(defn create-template ^IXDocReport [templateFile & options]
  (let [template (.loadReport (XDocReportRegistry/getRegistry) (io/input-stream templateFile) TemplateEngineKind/Freemarker)]
    (.removePreprocessor template DocxConstants/WORD_DOCUMENT_XML_ENTRY)
    (.addPreprocessor template DocxConstants/WORD_DOCUMENT_XML_ENTRY (fc/preprocessor))
    (.addPreprocessor template DocxConstants/WORD_DOCUMENT_XML_ENTRY DocxPreprocessor/INSTANCE)
    (when (some #(= % :pretty) options)
      (fc/format-archive-entry template DocxConstants/WORD_DOCUMENT_XML_ENTRY))
    template))

(defn fill-template [templateFile data outputFile & options]
  (process (create-template templateFile options) data outputFile)
  (when-let [merge-pattern (some :merge-pattern options)]
    (fc/merge-table-cells-in-doc outputFile merge-pattern)))

(defn competencies-mapped [competencies-map comps]
  (let [competenciesClassified (->> comps (competencies-with-codes competencies-map) (group-by #(competenceClass (get % "code"))))]
    {"ОК"  (get competenciesClassified "ОК" [])
     "УК"  (get competenciesClassified "УК" [])
     "ПК"  (get competenciesClassified "ПК" [])
     "ОПК" (get competenciesClassified "ОПК" [])
     "ПСК" (get competenciesClassified "ПСК" [])}))

