(defproject univ-doc-generator "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :java-source-paths ["src-java"]
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [com.github.kyleburton/clj-xpath "1.4.11"]
                 [fr.opensagres.xdocreport/fr.opensagres.xdocreport.document.docx "2.0.2"]
                 [fr.opensagres.xdocreport/fr.opensagres.xdocreport.template.freemarker "2.0.2"]
                 [org.freemarker/freemarker "2.3.29"]
                 [com.taoensso/timbre "4.10.0"]
                 [hawk "0.2.11"]
                 [org.clojure/data.xml "0.0.8"]
                 [dk.ative/docjure "1.13.0"]
                 [instaparse "1.4.9"]])
