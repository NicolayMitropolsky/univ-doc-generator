package univdocgen;


import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.OutputStream;

public class Formatter {


    public static void formatXml(Document xml, OutputStream outputStream) {
        format(new DOMSource(xml), new StreamResult(outputStream));
    }

    public static void formatXml(InputStream xml, OutputStream outputStream) {
        format(new StreamSource(xml), new StreamResult(outputStream));
    }

    public static void format(Source xmlSource, Result res) {
        try {
            Transformer serializer = SAXTransformerFactory.newInstance().newTransformer();
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            //serializer.setOutputProperty("{http://xml.customer.org/xslt}indent-amount", "2");
            serializer.transform(xmlSource, res);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}